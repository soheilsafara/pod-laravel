<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<form class="form-horizontal" method="post" action="{{ url('/products/add') }}">
    <fieldset>

        <!-- Form Name -->
        <legend>Form Name</legend>

        <!-- Text input-->
        <div class="form-group">

                <input id="name" name="name" type="text" placeholder="name" class="form-control input-md">
        </div>

        <!-- Text input-->
        <div class="form-group">

                <input id="availableCount" name="availableCount" type="text" placeholder="availableCount" class="form-control input-md">
        </div>


        <!-- Text input-->
        <div class="form-group">

                <input id="tags" name="tags" type="text" placeholder="tags" class="form-control input-md">
        </div>

        <!-- Text input-->
        <div class="form-group">

                <input id="price" name="price" type="text" placeholder="price" class="form-control input-md">
        </div>

        <!-- Text input-->
        <div class="form-group">

                <input id="discount" name="discount" type="text" placeholder="discount" class="form-control input-md">
        </div>

        <!-- Prepended checkbox -->
        <div class="form-group">
            canComment
            true
            <input type="radio" name="canComment" id="canComment" value="true">
            false
            <input type="radio" name="canComment" id="canComment" value="false">


        </div>
        <!-- Prepended checkbox -->
        <div class="form-group">
            canLike
            true
            <input type="radio" name="canLike" id="canLike" value="true">
            false
            <input type="radio" name="canLike" id="canLike" value="false">

        </div>




        <!-- Prepended checkbox -->
        <div class="form-group">
            enable
            true
            <input type="radio" name="enable" id="allowUserPrice" value="true">
            false
            <input type="radio" name="enable    " id="allowUserPrice" value="false">


        </div>
        <!-- Prepended checkbox -->
        <div class="form-group">
            unlimited
            true
            <input type="radio" name="unlimited" id="unlimited" value="true">
            false
            <input type="radio" name="unlimited" id="unlimited" value="false">

        </div>


        <!-- Text input-->
        <div class="form-group">

                <input id="description" name="description" type="text" placeholder="description" class="form-control input-md">
        </div>

        <!-- Text input-->
        <div class="form-group">

                <input id="content" name="content" type="text" placeholder="content" class="form-control input-md">

        </div>

        <!-- Text input-->
        <div class="form-group">

                <input id="attTemplateCode" name="attTemplateCode" type="text" placeholder="attTemplateCode" class="form-control input-md">

        </div>

            <input type="submit">
    </fieldset>
</form>

</body>
</html>
