<?php

namespace App\Http\Controllers;

use App\Exceptions\AddProductException;
use App\Http\Requests\addProduct;
use App\Services\PodServiceContract;
use Dotenv\Validator;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Request;
use Mockery\Exception;

class PodController extends Controller
{
    /**
     * @var PodServiceContract
     */
    private $podServiceContract;

    /**
     * PodController constructor.
     * @param PodServiceContract $podServiceContract
     */
    public function __construct(PodServiceContract $podServiceContract)
    {

        $this->podServiceContract = $podServiceContract;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function showProducts(Request $request)
    {

        $validator = \Validator::make($request->all(),[
            'size' => 'required|numeric',
            'offset' => 'required|numeric',
        ]);
        if ($validator->fails()) {

            return \response()->json(["error" => $validator->errors()]);

        }
        try {
            return \response()->json(["products : "=>$this->podServiceContract->showPosts($request['size'],$request['offset'])]);
        } catch (Exception $ex) {
            return \response()->json(["error" => $ex]);
        }
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addProductForm(){
        return view('addProduct');
    }
    /**
     * @param addProduct $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addProduct(addProduct $request)
    {

        try {
            $product = $this->podServiceContract->addProduct($request->all());
            if ($product && $product != -1){
                return \response()->json(["product : "=> $product]);
            }
            return \response()->json(["error" => "invalid response"]);
        } catch (Exception $ex) {
            return \response()->json(["error" => $ex]);
        }
    }
}

