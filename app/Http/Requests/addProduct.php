<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class addProduct extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => "required|string",
        "availableCount"=>"required|numeric",
        "price"=>"required|numeric",
        "description"=>"string",
        "discount"=>"required|numeric",
        "canComment"=>"required|in:true,false",
        "canLike"=>"required|in:true,false",
        "enable"=>"required|in:true,false",
        "tags"=>"string",
        "content"=>"string",
        "attTemplateCode"=>"string",
         "unlimited"=>"in:true,false"

        ];
    }
}
