<?php
namespace App\Services;

use App\Product;
use phpDocumentor\Reflection\Types\Boolean;

interface PodServiceContract
{
    function addProduct(array $product) : string  ;
    function showPosts(int $size, int $offset) : string ;

}
