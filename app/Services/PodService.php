<?php

namespace App\Services;

use App\Exceptions\AddProductException;
use App\Exceptions\ShowProductsException;
use http\Env\Response;
use Illuminate\Support\Facades\Http;
use phpDocumentor\Reflection\Types\False_;

class  PodService implements PodServiceContract
{

    /**
     * @param array $product
     * @return int
     */
    function addProduct(array $product): string
    {

        $response = Http::withHeaders([
            '_token_' =>env("POD_API_TOKEN"),
            '_token_issuer_' => '1'
        ])->asForm()->post('http://sandbox.pod.ir:8080/nzh/biz/addProduct', [
            'name' => $product['name'],
                    'canLike' => $product['canLike'],
                    'canComment' => $product['canComment'],
                    'enable' => $product['enable'],
                    'availableCount' => $product['availableCount'],
                    'price' => $product['price'],
                    'discount' => $product['discount'],
                    'description' => $product['description'],
                    'content' => $product['content'],
                    'attTemplateCode' => $product['attTemplateCode'],
                    'tags' => $product['tags']
        ]);
        if ($response && $response->ok()) {
            return $response->body();
        } else {
            return -1;
        }
    }

    /**
     * @param int $size
     * @param int $offset
     * @return string
     */
    function showPosts(int $size,int $offset): string
    {
        $response = Http::asForm()->withHeaders([
            '_token_' => env("POD_API_TOKEN"),
            '_token_issuer_' => 1
        ])->post('http://sandbox.pod.ir:8080/nzh/biz/productList/', [
            'size' => $size,
            'offset' => $offset
        ]);

        if ($response && $response->ok()) {

            return $response->body();
        } else {
            return [];
        }
    }
}


