<?php


namespace App\DTOs;


use Illuminate\Support\Collection;
use Psy\Util\Str;

class ProductDTO
{
    private $name;
    private $availableCount;
    private $price;
    private $description;
    private $discount;
    private $canComment;
    private $canLike;
    private $enable;
    private $tags;
    private $metadata;
    private $content;
    private $attTemplateCode;
    private $attCode;
    private $attValue;
    private $attGroup;
    private $groupId;
    private $unlimited;
    private $allowUserInvoice;
    private $allowUserPrice;


    /**
     * @return mixed
     */
    public function getName() : string
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getAvailableCount() : int
    {
        return $this->availableCount;
    }

    /**
     * @param mixed $availableCount
     */
    public function setAvailableCount(int $availableCount): void
    {
        $this->availableCount = $availableCount;
    }

    /**
     * @return mixed
     */
    public function getPrice() : float
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice(  float $price): void
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getDescription() : string
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription( string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getDiscount() : float
    {
        return $this->discount;
    }

    /**
     * @param mixed $discount
     */
    public function setDiscount( float $discount): void
    {
        $this->discount = $discount;
    }

    /**
     * @return mixed
     */
    public function getCanComment() : bool
    {
        return $this->canComment;
    }

    /**
     * @param mixed $canComment
     */
    public function setCanComment(bool $canComment): void
    {
        $this->canComment = $canComment;
    }

    /**
     * @return mixed
     */
    public function getCanLike() : bool
    {
        return $this->canLike;
    }

    /**
     * @param mixed $canLike
     */
    public function setCanLike(bool $canLike): void
    {
        $this->canLike = $canLike;
    }

    /**
     * @return mixed
     */
    public function getEnable() : bool
    {
        return $this->enable;
    }

    /**
     * @param mixed $enable
     */
    public function setEnable(bool $enable): void
    {
        $this->enable = $enable;
    }

    /**
     * @return mixed
     */
    public function getTags() : string
    {
        return $this->tags;
    }

    /**
     * @param mixed $tags
     */
    public function setTags(string $tags): void
    {
        $this->tags = $tags;
    }

    /**
     * @return mixed
     */
    public function getMetadata() : array
    {
        return $this->metadata;
    }

    /**
     * @param mixed $metadata
     */
    public function setMetadata(array $metadata): void
    {
        $this->metadata = $metadata;
    }



    /**
     * @return mixed
     */
    public function getContent() : string
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent( string $content): void
    {
        $this->content = $content;
    }

    /**
     * @return mixed
     */
    public function getAttTemplateCode() :string
    {
        return $this->attTemplateCode;
    }

    /**
     * @param mixed $attTemplateCode
     */
    public function setAttTemplateCode(string $attTemplateCode): void
    {
        $this->attTemplateCode = $attTemplateCode;
    }

    /**
     * @return mixed
     */
    public function getAttCode() : array
    {
        return $this->attCode;
    }

    /**
     * @param mixed $attCode
     */
    public function setAttCode(array $attCode): void
    {
        $this->attCode = $attCode;
    }

    /**
     * @return mixed
     */
    public function getAttValue() : array
    {
        return $this->attValue;
    }

    /**
     * @param mixed $attValue
     */
    public function setAttValue( array $attValue): void
    {
        $this->attValue = $attValue;
    }

    /**
     * @return mixed
     */
    public function getAttGroup() : array
    {
        return $this->attGroup;
    }

    /**
     * @param mixed $attGroup
     */
    public function setAttGroup(array $attGroup): void
    {
        $this->attGroup = $attGroup;
    }

    /**
     * @return mixed
     */
    public function getGroupId()  :int
    {
        return $this->groupId;
    }

    /**
     * @param mixed $groupId
     */
    public function setGroupId(int $groupId): void
    {
        $this->groupId = $groupId;
    }


    /**
     * @return mixed
     */
    public function getUnlimited() : bool
    {
        return $this->unlimited;
    }

    /**
     * @param mixed $unlimited
     */
    public function setUnlimited( bool $unlimited): void
    {
        $this->unlimited = $unlimited;
    }

    /**
     * @return mixed
     */
    public function getAllowUserInvoice() : bool
    {
        return $this->allowUserInvoice;
    }

    /**
     * @param mixed $allowUserInvoice
     */
    public function setAllowUserInvoice(bool $allowUserInvoice): void
    {
        $this->allowUserInvoice = $allowUserInvoice;
    }

    /**
     * @return mixed
     */
    public function getAllowUserPrice() : bool
    {
        return $this->allowUserPrice;
    }

    /**
     * @param mixed $allowUserPrice
     */
    public function setAllowUserPrice(bool $allowUserPrice): void
    {
        $this->allowUserPrice = $allowUserPrice;
    }

}
